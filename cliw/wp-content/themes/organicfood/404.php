<?php
/*
Template Name: 404 Page
*/
get_header(); ?>
<div class="container" id="error-page">
    <div>
        <h1 class="error-code">W-P-L-O-C-K-E-R-.-C-O-M<?php _e('404',THEMENAME);?></h1>
        <p class="error-message">
                <?php _e('Article not found',THEMENAME);?><br>
                <?php _e('Go Back',THEMENAME);?><a title="<?php _e('Home',THEMENAME);?>" href="<?php echo esc_url( home_url( '/'  ) );?>"><i class="fa fa-home"></i><?php _e('Home',THEMENAME);?></a>
        </p>
    </div>
</div>
<?php get_footer(); ?>
