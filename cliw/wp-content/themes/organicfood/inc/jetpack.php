<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package cshero
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function cshero_jetpack_setup() {
	load_theme_textdomain( THEMENAME, ABS_PATH . '/languages' );
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'cshero_jetpack_setup' );
