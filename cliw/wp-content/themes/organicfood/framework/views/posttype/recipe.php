<?php
// Register Custom Post Type
function cs_add_post_type_recipe() {
    // Register taxonomy
    $labels = array(
            'name'              => _x( 'Recipe Category', 'taxonomy general name', THEMENAME ),
            'singular_name'     => _x( 'Recipe Category', 'taxonomy singular name', THEMENAME ),
            'search_items'      => __( 'Search Recipe Category', THEMENAME ),
            'all_items'         => __( 'All Recipe Category', THEMENAME ),
            'parent_item'       => __( 'Parent Recipe Category', THEMENAME ),
            'parent_item_colon' => __( 'Parent Recipe Category:', THEMENAME ),
            'edit_item'         => __( 'Edit Recipe Category', THEMENAME ),
            'update_item'       => __( 'Update Recipe Category', THEMENAME ),
            'add_new_item'      => __( 'Add New Recipe Category', THEMENAME ),
            'new_item_name'     => __( 'New Recipe Category Name', THEMENAME ),
            'menu_name'         => __( 'Recipe Category', THEMENAME ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'recipe_category' ),
    );
    
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy('recipe_category', array( 'recipe' ), $args );
    }
    
    //Register tags
    $labels = array(
            'name'              => _x( 'Recipe Tag', 'taxonomy general name', THEMENAME ),
            'singular_name'     => _x( 'Recipe Tag', 'taxonomy singular name', THEMENAME ),
            'search_items'      => __( 'Search Recipe Tag', THEMENAME ),
            'all_items'         => __( 'All Recipe Tag', THEMENAME ),
            'parent_item'       => __( 'Parent Recipe Tag', THEMENAME ),
            'parent_item_colon' => __( 'Parent Recipe Tag:', THEMENAME ),
            'edit_item'         => __( 'Edit Recipe Tag', THEMENAME ),
            'update_item'       => __( 'Update Recipe Tag', THEMENAME ),
            'add_new_item'      => __( 'Add New Recipe Tag', THEMENAME ),
            'new_item_name'     => __( 'New Recipe Tag Name', THEMENAME ),
            'menu_name'         => __( 'Recipe Tag', THEMENAME ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'recipe_tag' ),
    );
    
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy( 'recipe_tag', array( 'recipe' ), $args );
    }
    
    //Register post type portfolio
    $labels = array(
            'name'                => _x( 'Recipe', 'Post Type General Name', THEMENAME ),
            'singular_name'       => _x( 'Recipe Item', 'Post Type Singular Name', THEMENAME ),
            'menu_name'           => __( 'Recipe', THEMENAME ),
            'parent_item_colon'   => __( 'Parent Item:', THEMENAME ),
            'all_items'           => __( 'All Items', THEMENAME ),
            'view_item'           => __( 'View Item', THEMENAME ),
            'add_new_item'        => __( 'Add New Item', THEMENAME ),
            'add_new'             => __( 'Add New', THEMENAME ),
            'edit_item'           => __( 'Edit Item', THEMENAME ),
            'update_item'         => __( 'Update Item', THEMENAME ),
            'search_items'        => __( 'Search Item', THEMENAME ),
            'not_found'           => __( 'Not found', THEMENAME ),
            'not_found_in_trash'  => __( 'Not found in Trash', THEMENAME ),
    );
    $args = array(
            'label'               => __( 'Recipe', THEMENAME ),
            'description'         => __( 'Recipe Description', THEMENAME ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments' ),
            'taxonomies'          => array( 'recipe_category', 'recipe_tag' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-pressthis',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
    );
    
    if(function_exists('custom_reg_post_type')) {
        custom_reg_post_type( 'recipe', $args );
    }

}

// Hook into the 'init' action
add_action( 'init', 'cs_add_post_type_recipe', 0 );
