<?php
// Register Custom Post Type
function cs_add_post_type_portfolio() {
    // Register taxonomy
    $labels = array(
            'name'              => _x( 'Portfolio Category', 'taxonomy general name', THEMENAME ),
            'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name', THEMENAME ),
            'search_items'      => __( 'Search Portfolio Category', THEMENAME ),
            'all_items'         => __( 'All Portfolio Category', THEMENAME ),
            'parent_item'       => __( 'Parent Portfolio Category', THEMENAME ),
            'parent_item_colon' => __( 'Parent Portfolio Category:', THEMENAME ),
            'edit_item'         => __( 'Edit Portfolio Category', THEMENAME ),
            'update_item'       => __( 'Update Portfolio Category', THEMENAME ),
            'add_new_item'      => __( 'Add New Portfolio Category', THEMENAME ),
            'new_item_name'     => __( 'New Portfolio Category Name', THEMENAME ),
            'menu_name'         => __( 'Portfolio Category', THEMENAME ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'portfolio_category' ),
    );
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );
    }
    //Register tags
    $labels = array(
            'name'              => _x( 'Portfolio Tag', 'taxonomy general name', THEMENAME ),
            'singular_name'     => _x( 'Portfolio Tag', 'taxonomy singular name', THEMENAME ),
            'search_items'      => __( 'Search Portfolio Tag', THEMENAME ),
            'all_items'         => __( 'All Portfolio Tag', THEMENAME ),
            'parent_item'       => __( 'Parent Portfolio Tag', THEMENAME ),
            'parent_item_colon' => __( 'Parent Portfolio Tag:', THEMENAME ),
            'edit_item'         => __( 'Edit Portfolio Tag', THEMENAME ),
            'update_item'       => __( 'Update Portfolio Tag', THEMENAME ),
            'add_new_item'      => __( 'Add New Portfolio Tag', THEMENAME ),
            'new_item_name'     => __( 'New Portfolio Tag Name', THEMENAME ),
            'menu_name'         => __( 'Portfolio Tag', THEMENAME ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'portfolio_tag' ),
    );
    
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy( 'portfolio_tag', array( 'portfolio' ), $args );
    }
    
    //Register post type portfolio
    $labels = array(
            'name'                => _x( 'Portfolio', 'Post Type General Name', THEMENAME ),
            'singular_name'       => _x( 'Portfolio Item', 'Post Type Singular Name', THEMENAME ),
            'menu_name'           => __( 'Portfolio', THEMENAME ),
            'parent_item_colon'   => __( 'Parent Item:', THEMENAME ),
            'all_items'           => __( 'All Items', THEMENAME ),
            'view_item'           => __( 'View Item', THEMENAME ),
            'add_new_item'        => __( 'Add New Item', THEMENAME ),
            'add_new'             => __( 'Add New', THEMENAME ),
            'edit_item'           => __( 'Edit Item', THEMENAME ),
            'update_item'         => __( 'Update Item', THEMENAME ),
            'search_items'        => __( 'Search Item', THEMENAME ),
            'not_found'           => __( 'Not found', THEMENAME ),
            'not_found_in_trash'  => __( 'Not found in Trash', THEMENAME ),
    );
    $args = array(
            'label'               => __( 'portfolio', THEMENAME ),
            'description'         => __( 'Portfolio Description', THEMENAME ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
            'taxonomies'          => array( 'portfolio_category', 'portfolio_tag' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-welcome-view-site',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
    );
    
    if(function_exists('custom_reg_post_type')) {
        custom_reg_post_type( 'portfolio', $args );
    }
    
}

// Hook into the 'init' action
add_action( 'init', 'cs_add_post_type_portfolio', 0 );
