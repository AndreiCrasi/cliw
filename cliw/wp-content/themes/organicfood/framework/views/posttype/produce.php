<?php

function ww_add_post_type_produce() {
    // Register taxonomy
    $labels = array(
            'name'              => _x( 'Produce Category', 'taxonomy general name', THEMENAME ),
            'singular_name'     => _x( 'Produce Category', 'taxonomy singular name', THEMENAME ),
            'search_items'      => __( 'Search Produce Category', THEMENAME ),
            'all_items'         => __( 'All Produce Category', THEMENAME ),
            'parent_item'       => __( 'Parent Produce Category', THEMENAME ),
            'parent_item_colon' => __( 'Parent Produce Category:', THEMENAME ),
            'edit_item'         => __( 'Edit Produce Category', THEMENAME ),
            'update_item'       => __( 'Update Produce Category', THEMENAME ),
            'add_new_item'      => __( 'Add New Produce Category', THEMENAME ),
            'new_item_name'     => __( 'New Produce Category Name', THEMENAME ),
            'menu_name'         => __( 'Produce Category', THEMENAME ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'produce_category' ),
    );
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy( 'produce_category', array( 'produce' ), $args );
    }
    //Register post type
    $labels = array(
        'name' => _x('Produce', 'Post type general name', THEMENAME),
        'singular_name' => _x('Produce', 'Post type singular name', THEMENAME),
        'add_new' => _x('Add New', 'Produce Item', THEMENAME),
        'add_new_item' => __('Add New Item', THEMENAME),
        'edit_item' => __('Edit Item', THEMENAME),
        'new_item' => __('New Item', THEMENAME),
        'all_items' => __('All Items', THEMENAME),
        'view_item' => __('View Item', THEMENAME),
        'search_items' => __('Search Items', THEMENAME),
        'not_found' => __('No produces found.', THEMENAME),
        'not_found_in_trash' => __('No produces found.', THEMENAME),
        'parent_item_colon' => '',
        'menu_name' => __('Produce', THEMENAME)
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-cart',
        'supports' => array('title', 'editor', 'thumbnail', 'comments')
    );
    
    if(function_exists('custom_reg_post_type')) {
        custom_reg_post_type( 'produce', $args );
    }
    
}
add_action('init', 'ww_add_post_type_produce');