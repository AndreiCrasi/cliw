<?php
foreach (glob("".get_template_directory()."/framework/includes/vc_extra_shortcodes/*.php") as $filename)
{
	include $filename;
}

vc_remove_element ( "vc_cta_button2" );
vc_remove_element ( "vc_button2" );
// intergrate VC
add_action ( 'init', 'cs_integrateWithVC' );
function cs_integrateWithVC() {
	$vc_is_wp_version_3_6_more = version_compare ( preg_replace ( '/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo ( 'version' ) ), '3.6' ) >= 0;
	vc_map ( array (
			'name' => __( 'Pie chart', 'vc_extend' ),
			'base' => 'vc_pie',
			'class' => '',
			'icon' => 'icon-wpb-vc_pie',
			'category' => __( 'Content', THEMENAME ),
			'description' => __( 'Animated pie chart', THEMENAME ),
			'params' => array (
					array (
							'type' => 'textfield',
							'heading' => __( 'Widget title', THEMENAME ),
							'param_name' => 'title',
							'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', THEMENAME ),
							'admin_label' => true
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Pie value', THEMENAME ),
							'param_name' => 'value',
							'description' => __( 'Input graph value here. Choose range between 0 and 100.', THEMENAME ),
							'value' => '50',
							'admin_label' => true
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Pie icon', THEMENAME ),
							'param_name' => 'icon',
							'description' => __( '', THEMENAME ),
							'value' => '',
							'admin_label' => true
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Pie label value', THEMENAME ),
							'param_name' => 'label_value',
							'description' => __( 'Input integer value for label. If empty "Pie value" will be used.', THEMENAME ),
							'value' => ''
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Units', THEMENAME ),
							'param_name' => 'units',
							'description' => __( 'Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.', THEMENAME )
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Bar color', THEMENAME ),
							'param_name' => 'color',
							'value' => '#00c3b6', // $pie_colors,
							'description' => __( 'Select pie chart color.', THEMENAME ),
							'admin_label' => true,
							'param_holder_class' => 'vc-colored-dropdown'
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Extra class name', THEMENAME ),
							'param_name' => 'el_class',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', THEMENAME )
					)
			)

	) );
	/*
	 * Tabs ----------------------------------------------------------
	 */
	$tab_id_1 = time () . '-1-' . rand ( 0, 100 );
	$tab_id_2 = time () . '-2-' . rand ( 0, 100 );
	vc_map ( array (
			"name" => __( 'Tabs', THEMENAME ),
			'base' => 'vc_tabs',
			'show_settings_on_create' => false,
			'is_container' => true,
			'icon' => 'icon-wpb-ui-tab-content',
			'category' => __( 'Content', THEMENAME ),
			'description' => __( 'Tabbed content', THEMENAME ),
			'params' => array (
					array (
							'type' => 'textfield',
							'heading' => __( 'Widget title', THEMENAME ),
							'param_name' => 'title',
							'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', THEMENAME )
					),
					array (
							'type' => 'dropdown',
							'heading' => __( 'Auto rotate tabs', THEMENAME ),
							'param_name' => 'interval',
							'value' => array (
									__( 'Disable', THEMENAME ) => 0,
									3,
									5,
									10,
									15
							),
							'std' => 0,
							'description' => __( 'Auto rotate tabs each X seconds.', THEMENAME )
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Extra class name', THEMENAME ),
							'param_name' => 'el_class',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', THEMENAME )
					),
					array (
							'type' => 'dropdown',
							'param_name' => 'style',
							'heading' => __( 'Style', THEMENAME ),
							'value' => array (
									"Style 1" => "style1",
									"Style 2" => "style2",
									"Style 3" => "style3"
							),
							'std' => 'style1',
							'description' => __( '', THEMENAME )
					)
			),
			'custom_markup' => '
	<div class="wpb_tabs_holder wpb_holder vc_container_for_children">
	<ul class="tabs_controls">
	</ul>
	%content%
	</div>',
			'default_content' => '
	[vc_tab title="' . __( 'Tab 1', THEMENAME ) . '" tab_id="' . $tab_id_1 . '"][/vc_tab]
	[vc_tab title="' . __( 'Tab 2', THEMENAME ) . '" tab_id="' . $tab_id_2 . '"][/vc_tab]
	',
			'js_view' => $vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35'
	) );
        vc_map( array(
            'name' => __( 'Tab', THEMENAME ),
            'base' => 'vc_tab',
            'allowed_container_element' => 'vc_row',
            'is_container' => true,
            'content_element' => false,
            'params' => array(
                    array(
                            'type' => 'textfield',
                            'heading' => __( 'Title', THEMENAME ),
                            'param_name' => 'title',
                            'description' => __( 'Tab title.', THEMENAME )
                    ),
                    array(
                            'type' => 'textfield',
                            'heading' => __( 'Icon', THEMENAME ),
                            'param_name' => 'icon',
                            'description' => __( 'Icon class.', THEMENAME )
                    ),
                    array(
                            'type' => 'tab_id',
                            'heading' => __( 'Tab ID', THEMENAME ),
                            'param_name' => "tab_id"
                    )
            ),
            'js_view' => $vc_is_wp_version_3_6_more ? 'VcTabView' : 'VcTabView35'
    ) );
	/*
	 * Call to Action Button ----------------------------------------------------------
	 */
	$target_arr = array (
			__( 'Same window', THEMENAME ) => '_self',
			__( 'New window', THEMENAME ) => "_blank"
	);
	$button_type = array (
			__( 'Button Default', THEMENAME ) => 'btn-default',
			__( 'Button Primary', THEMENAME ) => 'btn-primary',
			__( 'Button Default Link', THEMENAME ) => 'btn-default btn-link',
			__( 'Button Primary link', THEMENAME ) => 'btn btn-primary btn-link-primary',
			__( 'Button Primary White', THEMENAME ) => 'btn btn-trans btn-white'
	);
	$size_arr = array (
			__( 'Regular size', THEMENAME ) => '',
			__( 'Large', THEMENAME ) => 'btn-large',
			__( 'Small', THEMENAME ) => 'btn-small',
			__( 'Mini', THEMENAME ) => "btn-mini"
	);
	$add_css_animation = array (
			'type' => 'dropdown',
			'heading' => __ ( 'CSS Animation', THEMENAME ),
			'param_name' => 'css_animation',
			'admin_label' => true,
			'value' => array (
					__( 'No', THEMENAME ) => '',
					__( 'Top to bottom', THEMENAME ) => 'top-to-bottom',
					__( 'Bottom to top', THEMENAME ) => 'bottom-to-top',
					__( 'Left to right', THEMENAME ) => 'left-to-right',
					__( 'Right to left', THEMENAME ) => 'right-to-left',
					__( 'Appear from center', THEMENAME ) => "appear"
			),
			'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', THEMENAME )
	);
	vc_map ( array (
			'name' => __( 'Call to Action Button', THEMENAME ),
			'base' => 'vc_cta_button',
			'icon' => 'icon-wpb-call-to-action',
			'category' => __( 'Content', THEMENAME ),
			'description' => __( 'Catch visitors attention with CTA block', THEMENAME ),
			'params' => array (
					array (
							'type' => 'textarea',
							'admin_label' => true,
							'heading' => __( 'Text', THEMENAME ),
							'param_name' => 'call_text',
							'value' => __( 'Click edit button to change this text.', THEMENAME ),
							'description' => __( 'Enter your content.', THEMENAME )
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'Text on the button', THEMENAME ),
							'param_name' => 'title',
							'value' => __( 'Text on the button', THEMENAME ),
							'description' => __( 'Text on the button.', THEMENAME )
					),
					array (
							'type' => 'textfield',
							'heading' => __( 'URL (Link)', THEMENAME ),
							'param_name' => 'href',
							'description' => __( 'Button link.', THEMENAME )
					),
					array (
							'type' => 'dropdown',
							'heading' => __( 'Target', THEMENAME ),
							'param_name' => 'target',
							'value' => $target_arr,
							'dependency' => array (
									'element' => 'href',
									'not_empty' => true
							)
					),
					array (
							'type' => 'dropdown',
							'heading' => __( 'Button Type ', THEMENAME ),
							'param_name' => 'button_type',
							'value' => $button_type,
							'description' => __( 'Button Type.', THEMENAME ),
							'param_holder_class' => 'vc-button-type-dropdown'
					),
					array (
							'type' => 'dropdown',
							'heading' => __( 'Size', THEMENAME ),
							'param_name' => 'size',
							'value' => $size_arr,
							'description' => __( 'Button size.', THEMENAME )
					),
					array (
							'type' => 'dropdown',
							'heading' => __( 'Button position', THEMENAME ),
							'param_name' => 'position',
							'value' => array (
									__( 'Align right', THEMENAME ) => 'cs_align_right',
									__( 'Align left', THEMENAME ) => 'cs_align_left'
							),
							'description' => __( 'Select button alignment.', THEMENAME )
					),
					$add_css_animation,
					array (
							'type' => 'textfield',
							'heading' => __( 'Extra class name', THEMENAME ),
							'param_name' => 'el_class',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', THEMENAME )
					)
			),
			'js_view' => 'VcCallToActionView'
	) );
}