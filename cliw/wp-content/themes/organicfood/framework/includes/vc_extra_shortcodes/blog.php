<?php

add_action('init', 'blog_integrateWithVC');

function blog_integrateWithVC() {
    vc_map(array(
        "name" => __("Blog", THEMENAME),
        "base" => "blog",
        "class" => "blog",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Post Type", THEMENAME),
                "param_name" => "post_type",
                "value" => array(
                    "Post" => "post",
                    "Recipe" => "recipe",
                    "Team" => "team",
                ),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Post Count", THEMENAME),
                "param_name" => "posts_per_page",
                "value" => "",
            ),
            array (
                "type" => "pro_taxonomy",
                "taxonomy" => "category",
                "dependency" => array(
                    "element"=>"post_type",
                    "value"=>"post"
                    )
                ,
                "heading" => __ ( "Categories", THEMENAME ),
                "param_name" => "category",
                "class" => "",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", THEMENAME, THEMENAME )
            ),
            array (
                "type" => "pro_taxonomy",
                "taxonomy" => "team_category",
                "dependency" => array(
                    "element"=>"post_type",
                    "value"=>"team"
                    )
                ,
                "class" => "",
                "heading" => __ ( "Categories", THEMENAME ),
                "param_name" => "team_category",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", THEMENAME, THEMENAME )
            ),
            array (
                "type" => "pro_taxonomy",
                "taxonomy" => "recipe_category",
                "dependency" => array(
                    "element"=>"post_type",
                    "value"=>"recipe"
                    )
                ,
                "class" => "",
                "heading" => __ ( "Categories", THEMENAME ),
                "param_name" => "recipe_category",
                "description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", THEMENAME, THEMENAME )
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Extra Screen", THEMENAME),
                "param_name" => "col_xs",
                "value" => array(
                    "1 Column" => "col-sx-12",
                    "2 Columns" => "col-sx-6",
                    "3 Columns" => "col-sx-4",
                    "4 Columns" => "col-sx-3",
                    "6 Columns" => "col-sx-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Small Screen", THEMENAME),
                "param_name" => "col_sm",
                "value" => array(
                    "1 Column" => "col-sm-12",
                    "2 Columns" => "col-sm-6",
                    "3 Columns" => "col-sm-4",
                    "4 Columns" => "col-sm-3",
                    "6 Columns" => "col-sm-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Medium Screen", THEMENAME),
                "param_name" => "col_md",
                "value" => array(
                    "1 Column" => "col-md-12",
                    "2 Columns" => "col-md-6",
                    "3 Columns" => "col-md-4",
                    "4 Columns" => "col-md-3",
                    "6 Columns" => "col-md-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Large Screen", THEMENAME),
                "param_name" => "col_lg",
                "value" => array(
                    "1 Column" => "col-lg-12",
                    "2 Columns" => "col-lg-6",
                    "3 Columns" => "col-lg-4",
                    "4 Columns" => "col-lg-3",
                    "6 Columns" => "col-lg-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "styles",
                "value" => array(
                    "Style 1" => "style1",
                ),
                "dependency" => array(
                    "element"=>"post_type",
                    "value"=>"post"
                    )
                ,
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "team_styles",
                "value" => array(
                    "Style Image Left" => "style1",
                    "Style Image Right" => "style2",
                    "Style Image Top" => "style3",
                ),
                "dependency" => array(
                    "element"=>"post_type",
                    "value"=>"team"
                    )
                ,
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "recipe_styles",
                "value" => array(
                    "Style 1" => "style1",
                ),
                "dependency" => array(
                    "element"=>"post_type",
                    "value"=>"recipe"
                    )
                ,
            ),
            array(
                "type" => "checkbox", 
                "heading" => __('Crop image', THEMENAME),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "description" => __('Crop or not crop image on your Post.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', THEMENAME),
                "param_name" => "width_image",
                "description" => __('Enter the width of image. Default: 300.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', THEMENAME),
                "param_name" => "height_image",
                "description" => __('Enter the height of image. Default: 200.', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', THEMENAME),
                "param_name" => "show_title",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "description" => __('Show or hide title of post on your blog.', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Info', THEMENAME),
                "param_name" => "show_info",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "description" => __('Show or hide info of post on your blog.', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Description', THEMENAME),
                "param_name" => "show_description",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "dependency" => array(
                    "element" => "post_type",
                    "value" => "post"
                ),
                "description" => __('Show or hide description of post on your blog.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt Length', THEMENAME),
                "param_name" => "excerpt_length",
                "value" => '',
                "dependency" => array(
                    "element" => "post_type",
                    "value" => "post"
                ),
                "description" => __('The length of the excerpt, number of words to display.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt More', THEMENAME),
                "param_name" => "excerpt_more",
                "value" => "",
                "dependency" => array(
                    "element" => "post_type",
                    "value" => "post"
                ),
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Extra Excerpt', THEMENAME),
                "param_name" => "show_extra_excerpt",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "dependency" => array(
                    "element" => "post_type",
                    "value" => "recipe"
                ),
                "description" => __('Show or hide extra recipt of post on your blog.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Read More Text", THEMENAME),
                "param_name" => "readmore_text",
                "value" => "",
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order by', THEMENAME),
                "param_name" => "orderby",
                "value" => array(
                    "None" => "none",
                    "Title" => "title",
                    "Date" => "date",
                    "ID" => "ID"
                ),
                "description" => __('Order by ("none", "title", "date", "ID").', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', THEMENAME),
                "param_name" => "order",
                "value" => Array(
                    "None" => "none",
                    "ASC" => "ASC",
                    "DESC" => "DESC"
                ),
                "description" => __('Order ("None", "Asc", "Desc").', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Pagination', THEMENAME),
                "param_name" => "show_pagination",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "description" => __('Show or hide pagination of post on your blog.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
            ),
        )
    ));
}
