<?php
vc_map ( array (
		"name" => 'Drop Caps',
		"base" => "cs-dropcap",
		"icon" => "of-icon-for-vc",
		"category" => __ ( 'Organic Food', THEMENAME ),
		"params" => array (
				array (
						"type" => "textarea_html",
                                                "holder" => "div",
						"heading" => __ ( 'Content', THEMENAME ),
						"param_name" => "content",
						"value" => '',
				)
		)
) );