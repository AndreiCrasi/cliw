<?php
vc_map ( array (
		"name" => 'Zoom Image',
		"base" => "cs-zoomimg",
		"icon" => "of-icon-for-vc",
		"category" => __ ( 'Organic Food', THEMENAME ),
		"params" => array (
                    array (
                        "type" => "attach_image",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __ ( "Image", THEMENAME ),
                        "param_name" => "image"
                    ),
		)
) );