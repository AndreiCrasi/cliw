<?php

add_action('init', 'featured_box_integrateWithVC');

function featured_box_integrateWithVC() {
    vc_map(array(
        "name" => __("Featured Box", THEMENAME),
        "base" => "featured-box",
        "class" => "featured-box",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Box Type", THEMENAME),
                "param_name" => "box_type",
                "value" => array(
                    "Text" => "text",
                    "Icon" => "icon",
                    "Image" => "image",
                ),
                "description" => __('Box Type', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Box Position", THEMENAME),
                "param_name" => "box_position",
                "value" => array(
                    "Box Left" => "box-left",
                    "Box Right" => "box-right",
                    "Box Center" => "box-center",
                ),
                "description" => __('Box Position', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Box Inner Type", THEMENAME),
                "param_name" => "box_inner_type",
                "value" => array(
                    "None" => "box-none",
                    "Square" => "box-square",
                    "Circle" => "box-circle",
                    
                ),
                "dependency" => array(
                    "element"=>"box_type",
                    "value"=> array("text", "icon")
                ),
                "description" => __('Box Inner Type', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Box Style", THEMENAME),
                "param_name" => "box_style",
                "value" => array(
                    "None" => "",
                    "Background" => "box-background",
                    "Border" => "box-border",
                    
                ),
                "dependency" => array(
                    "element"=>"box_inner_type",
                    "value"=> array("box-square", "box-circle")
                ),
                "description" => __('Box Style.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Box Text", THEMENAME),
                "param_name" => "box_text",
                "value" => "",
                "dependency" => array(
                    "element"=>"box_type",
                    "value"=>"text"
                ),
                "description" => __("Box Text.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Box Icon", THEMENAME),
                "param_name" => "box_icon",
                "value" => "",
                "dependency" => array(
                    "element"=>"box_type",
                    "value"=>"icon"
                ),
                "description" => __("Box Icon.", THEMENAME)
            ),
            array(
                "type" => "attach_image",
                "holder" => "",
                "class" => "",
                "heading" => __("Box Image", THEMENAME),
                "param_name" => "box_image",
                "value" => "",
                "dependency" => array(
                    "element"=>"box_type",
                    "value"=>"image"
                ),
                "description" => __("Box Image.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Box Title", THEMENAME),
                "param_name" => "box_title",
                "value" => "",
                "description" => __("Box Title.", THEMENAME)
            ), 
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => __("Box Content", THEMENAME),
                "param_name" => "content",
                "value" => "",
                "description" => __("Box Content.", THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", THEMENAME),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
                "description" => __("Animation", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
