<?php

add_action('init', 'icon_integrateWithVC');

function icon_integrateWithVC() {
    vc_map(array(
        "name" => __("Icon", THEMENAME),
        "base" => "icon",
        "class" => "icon",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __("Color", THEMENAME),
                "param_name" => "color",
                "value" => "",
                "description" => __("Color.", THEMENAME)
            ),        
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Font size", THEMENAME),
                "param_name" => "fontsize",
                "value" => "",
                "description" => __("Font size.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Link", THEMENAME),
                "param_name" => "link",
                "value" => "",
                "description" => __("Link.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Class", THEMENAME),
                "param_name" => "class",
                "value" => "",
                "description" => __("Class.", THEMENAME)
            ),
            array(
                "type" => "textarea",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text", THEMENAME),
                "param_name" => "content",
                "value" => "",
                "description" => __("Text.", THEMENAME)
            )
        )
    ));
}
