<?php

add_action('init', 'panel_box_integrateWithVC');

function panel_box_integrateWithVC() {
    vc_map(array(
        "name" => __("Panel Box", THEMENAME),
        "base" => "panel-box",
        "class" => "panel-box",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Icon", THEMENAME),
                "param_name" => "icon",
                "value" => "fa fa-plus-circle",
                "description" => __("Icon.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Tooltip Title", THEMENAME),
                "param_name" => "tooltip_title",
                "value" => "",
                "description" => __("Tooltip Title.", THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Tooltip Position", THEMENAME),
                "param_name" => "tooltip_pos",
                "value" => array(
                    "Top" => 'top',
                    "Right" => 'right',
                    "Bottom" => 'bottom',
                    "Left" => 'left'
                ),
                "description" => __("Tooltip Position.", THEMENAME),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", THEMENAME),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
                "description" => __("Animation", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
