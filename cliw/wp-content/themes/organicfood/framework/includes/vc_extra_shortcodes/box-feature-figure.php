<?php

add_action('init', 'box_feature_figure_integrateWithVC');

function box_feature_figure_integrateWithVC() {
    vc_map(array(
        "name" => __("Box Feature Figure", THEMENAME),
        "base" => "box-feature-figure",
        "class" => "box-feature-figure",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "style",
                "value" => array(
                    "Feature Box" => "box-feature",
                    "Service Box" => "service-box"
                ),
            ),
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => __("Image", THEMENAME),
                "param_name" => "image",
                "value" => "",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Icon", THEMENAME),
                "param_name" => "icon",
                "value" => "",
            ),
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => __("Content", THEMENAME),
                "param_name" => "content",
                "value" => "",
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", THEMENAME),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
            ),
        )
    ));
}
