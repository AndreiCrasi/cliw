<?php
vc_map ( array (
		"name" => 'Bubble',
		"base" => "bubble",
		"icon" => "of-icon-for-vc",
		"category" => __ ( 'Organic Food', THEMENAME ),
		"params" => array (
				array (
						"type" => "textfield",
                                                "holder" => "div",
						"heading" => __ ( 'Author', THEMENAME ),
						"param_name" => "author",
						"value" => '',
				),
				array (
						"type" => "colorpicker",
						"heading" => __ ( 'Color', THEMENAME ),
						"param_name" => "color",
						"value" => '',
				),
				array (
						"type" => "colorpicker",
						"heading" => __ ( 'Background', THEMENAME ),
						"param_name" => "background",
						"value" => '',
				),
				array (
						"type" => "textfield",
						"heading" => __ ( 'Border Width', THEMENAME ),
						"param_name" => "border_width",
						"value" => '',
				),
				array (
						"type" => "colorpicker",
						"heading" => __ ( 'Border Color', THEMENAME ),
						"param_name" => "border_color",
						"value" => '',
				),
				array (
						"type" => "dropdown",
						"heading" => __ ( 'Border Style', THEMENAME ),
						"param_name" => "border_style",
						"value" => array('none','hidden','dotted','dashed','solid','double','groove','ridge','inset','outset','initial','inherit'),
				),
				array (
						"type" => "textfield",
						"heading" => __ ( 'Padding', THEMENAME ),
						"param_name" => "padding",
						"value" => '',
				),
				array (
						"type" => "textarea_html",
						"heading" => __ ( 'Content', THEMENAME ),
						"param_name" => "content",
						"value" => '',
				)
		)
) );