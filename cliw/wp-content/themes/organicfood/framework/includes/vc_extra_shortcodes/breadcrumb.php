<?php

add_action('init', 'breadcrumb_integrateWithVC');

function breadcrumb_integrateWithVC() {
    vc_map(array(
        "name" => __("Breadcrumb", THEMENAME),
        "base" => "breadcrumb",
        "class" => "breadcrumb",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __("Delimiter", THEMENAME),
                    "param_name" => "delimiter",
                    "value" => "",
                    "description" => __("Enter delimiter page breadcrumb.", THEMENAME)
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __("Color", THEMENAME),
                "param_name" => "color",
                "value" => "",
                "description" => __("Color.", THEMENAME)
            ),
            array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __("Extra Class", THEMENAME),
                    "param_name" => "el_class",
                    "value" => "",
            ),
        )
    ));
}
