<?php
vc_map ( array (
		"name" => 'Block Text',
		"base" => "block-text",
		"icon" => "of-icon-for-vc",
		"category" => __ ( 'Organic Food', THEMENAME ),
		"params" => array (
				array (
						"type" => "dropdown",
						"heading" => __ ( 'Type', THEMENAME ),
						"param_name" => "type",
						"value" => array(
                                                    "Default" => "default",
                                                    "Rounded" => "rounded",
                                                ),
						"description" =>  __ ( 'Type', THEMENAME )
				),
				array (
						"type" => "colorpicker",
						"heading" => __ ( 'Color', THEMENAME ),
						"param_name" => "color",
						"value" => '',
						"description" => __ ( 'Color', THEMENAME ),
				),
				array (
						"type" => "colorpicker",
						"heading" => __ ( 'Background', THEMENAME ),
						"param_name" => "background",
						"value" => '',
						"description" => __ ( 'Background', THEMENAME ),
				),
				array (
						"type" => "textfield",
						"heading" => __ ( 'Border Width', THEMENAME ),
						"param_name" => "border_width",
						"value" => '',
						"description" => __( 'Border Width', THEMENAME ),
				),
				array (
						"type" => "colorpicker",
						"heading" => __ ( 'Border Color', THEMENAME ),
						"param_name" => "border_color",
						"value" => '',
						"description" => __ ( 'Border Color', THEMENAME ),
				),
				array (
						"type" => "dropdown",
						"heading" => __ ( 'Border Style', THEMENAME ),
						"param_name" => "border_style",
						"value" => array('none','hidden','dotted','dashed','solid','double','groove','ridge','inset','outset','initial','inherit'),
						"description" => __ ( 'Border Style', THEMENAME ),
				),
				array (
						"type" => "textfield",
						"heading" => __ ( 'Padding', THEMENAME ),
						"param_name" => "padding",
						"value" => '',
						"description" =>  __ ( 'Padding', THEMENAME ),
				),
				array (
						"type" => "textarea_html",
                                                "holder" => "div",
						"heading" => __ ( 'Content', THEMENAME ),
						"param_name" => "content",
						"value" => '',
						"description" => __ ( 'Content', THEMENAME ),
				)
		)
) );