<?php

add_action('init', 'piegraph_integrateWithVC');

function piegraph_integrateWithVC() {
    vc_map(array(
        "name" => __("Pie Graph", THEMENAME),
        "base" => "ww-shortcode-piegraph",
        "class" => "piegraph",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title.", THEMENAME)
            ),         
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Percent", THEMENAME),
                "param_name" => "percent",
                "value" => "",
                "description" => __("Percent.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Radius", THEMENAME),
                "param_name" => "radius",
                "value" => "",
                "description" => __("Radius.", THEMENAME)
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __("Stroke color", THEMENAME),
                "param_name" => "color",
                "value" => '#FFFFFF',
                "description" => __("Choose stroke color", THEMENAME)
             ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
