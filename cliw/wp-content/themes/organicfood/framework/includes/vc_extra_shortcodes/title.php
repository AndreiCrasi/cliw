<?php

add_action('init', 'title_integrateWithVC');

function title_integrateWithVC() {
    vc_map(array(
        "name" => __("Title", THEMENAME),
        "base" => "title",
        "class" => "title",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
                "description" => __("Content.", THEMENAME)
            ),
            array (
                "type" => "colorpicker",
                "heading" => __ ( 'Color', THEMENAME ),
                "param_name" => "color",
                "value" => '',
                "description" => __ ( 'Color', THEMENAME ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Align", THEMENAME),
                "param_name" => "align",
                "value" => array(
                    "Left" => "text-left",
                    "Right" => "text-right",
                    "Center" => "text-center"
                ),
                "description" => __("Align", THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "class" => "",
                "heading" => __("Underline", THEMENAME),
                "param_name" => "underline",
                "value" => array (
                                __ ( "Yes, please", THEMENAME ) => 1
                ),
                "description" => __("Underline.", THEMENAME)
            ),            
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", THEMENAME),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
                "description" => __("Animation", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
