<?php
if (class_exists ( 'Woocommerce' )) {
	vc_map ( array (
			"name" => 'Products Carousel',
			"base" => "cs-products-carousel",
			"icon" => "of-icon-for-vc",
			"category" => __ ( 'Organic Food', THEMENAME ),
			"params" => array (
					array (
							"type" => "dropdown",
							"class" => "",
							"heading" => __ ( "Style", THEMENAME ),
							"param_name" => "style",
							"value" => array (
									"Default" => "default",
							),
							"description" => __ ( "", THEMENAME )
					),
					array (
							"type" => "pro_taxonomy",
							"taxonomy" => "product_cat",
							"heading" => __ ( "Categories", THEMENAME ),
							"param_name" => "product_cat",
							"description" => __ ( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", THEMENAME )
					),
                                        array (
							"type" => "dropdown",
							"class" => "",
							"heading" => __ ( "Show", THEMENAME ),
							"param_name" => "show",
							"value" => array (
									"All Products" => "",
									"Featured Products" => "featured",
									"On-sale Products" => "onsale",
							),
							"description" => __ ( "", THEMENAME )
					),
                                        array (
							"type" => "textfield",
							"heading" => __ ( 'Number of products to show', THEMENAME ),
							"param_name" => "number",
							'value' => 12,
							"description" => __ ( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', THEMENAME )
					),
                                        array (
							"type" => "dropdown",
							"class" => "",
							"heading" => __ ( "Rows", THEMENAME ),
							"param_name" => "rows",
							"value" => array (
									"1 Row" => 1,
									"2 Rows" => 2,
									"3 Rows" => 3,
									"4 Rows" => 4
							),
							"description" => __ ( "", THEMENAME )
					),
					array (
							"type" => "textfield",
							"heading" => __ ( 'Width Item', THEMENAME ),
							"param_name" => "width_item",
							"description" => __ ( 'Enter the width of item. Default: 150.', THEMENAME )
					),
					array (
							"type" => "textfield",
							"heading" => __ ( 'Margin Item', THEMENAME ),
							"param_name" => "margin_item",
							"description" => __ ( 'Enter the margin of item. Default: 20.', THEMENAME )
					),
					array (
							"type" => "textfield",
							"heading" => __ ( 'Speed Scroll', THEMENAME ),
							"param_name" => "speed",
							"description" => __ ( 'Enter the speed of carousel. Default: 500.', THEMENAME )
					),
					array (
							"type" => "checkbox",
							"heading" => __ ( 'Auto Scroll', THEMENAME ),
							"param_name" => "auto_scroll",
							"value" => array (
									__ ( "Yes, please", THEMENAME ) => 1
							),
							"description" => __ ( 'Auto scroll.', THEMENAME )
					),
                                        array (
							"type" => "checkbox",
							"heading" => __ ( 'Same Height', THEMENAME ),
							"param_name" => "same_height",
							"value" => array (
									__ ( "Yes, please", THEMENAME ) => 1
							),
							"description" => __ ( 'Same height item.', THEMENAME )
					),
					array (
							"type" => "checkbox",
							"heading" => __ ( 'Show Navigation', THEMENAME ),
							"param_name" => "show_nav",
							"value" => array (
									__ ( "Yes, please", THEMENAME ) => 1
							),
							"description" => __ ( 'Show or hidden navigation.', THEMENAME )
					),
					array (
							"type" => "checkbox",
							"heading" => __ ( 'Show Title', THEMENAME ),
							"param_name" => "show_title",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden title on your Product.', THEMENAME )
					),
                                        array (
							"type" => "checkbox",
							"heading" => __ ( 'Show Price', THEMENAME ),
							"param_name" => "show_price",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden price on your Product.', THEMENAME )
					),
                                        array (
							"type" => "checkbox",
							"heading" => __ ( 'Show Rating', THEMENAME ),
							"param_name" => "show_rating",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden rating on your Product.', THEMENAME )
					),
					array (
							"type" => "checkbox",
							"heading" => __ ( 'Show Category', THEMENAME ),
							"param_name" => "show_category",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden category on your Product.', THEMENAME )
					),
					array (
							"type" => "checkbox",
							"heading" => __ ( 'Show Add To Cart', THEMENAME ),
							"param_name" => "show_add_to_cart",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden add to cart on your Product.', THEMENAME )
					),
					array (
							"type" => "dropdown",
							"heading" => __ ( 'Order by', THEMENAME ),
							"param_name" => "orderby",
							"value" => array (
									"None" => "none",
									"Date" => "date",
                                                                        "Price" => "price",
									"Random" => "rand",
                                                                        "Sales" => "sales",
							),
							"description" => __ ( 'Order by ("none", "date", "price", "rand", "sales").', THEMENAME )
					),
					array (
							"type" => "dropdown",
							"heading" => __ ( 'Order', THEMENAME ),
							"param_name" => "order",
							"value" => array (
									"None" => "none",
									"Ascending" => "asc",
									"Descending" => "desc"
							),
							"description" => __ ( 'Order ("none", "asc", "desc").', THEMENAME )
					),
                                        array (
							"type" => "checkbox",
							"heading" => __ ( 'Hide free products', THEMENAME ),
							"param_name" => "hide_free",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden free products.', THEMENAME )
					),
                                        array (
							"type" => "checkbox",
							"heading" => __ ( 'Show hidden products', THEMENAME ),
							"param_name" => "show_hidden",
							"value" => array (
									__ ( "Yes, please", "js_composer" ) => 1
							),
							"description" => __ ( 'Show or hidden products.', THEMENAME )
					),
			)
	) );
}