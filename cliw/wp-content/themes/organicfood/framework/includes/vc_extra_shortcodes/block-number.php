<?php

add_action('init', 'block_number_integrateWithVC');

function block_number_integrateWithVC() {
    vc_map(array(
        "name" => __("Block Number", THEMENAME),
        "base" => "block-number",
        "class" => "block-number",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Type", THEMENAME),
                "param_name" => "type",
                "value" => array(
                    "Square" => "square",
                    "Circle" => "circle",
                    "Rounded" => "rounded",
                ),
                "description" => __('Type', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text", THEMENAME),
                "param_name" => "text",
                "value" => "",
                "description" => __("Text.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title.", THEMENAME)
            ),
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => __("Content", THEMENAME),
                "param_name" => "block_number_content",
                "value" => "",
                "description" => __("content.", THEMENAME)
            ),          
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __("Color", THEMENAME),
                "param_name" => "color",
                "value" => "",
                "description" => __("Color.", THEMENAME)
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __("Background", THEMENAME),
                "param_name" => "background",
                "value" => "",
                "description" => __("background.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
