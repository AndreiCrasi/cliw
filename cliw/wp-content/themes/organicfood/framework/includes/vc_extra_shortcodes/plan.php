<?php

add_action('init', 'plan_integrateWithVC');

function plan_integrateWithVC() {
    vc_map(array(
        "name" => __("Plan", THEMENAME),
        "base" => "ww-shortcode-plan",
        "class" => "plan",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Name", THEMENAME),
                "param_name" => "name",
                "value" => "",
                "description" => __("Name.", THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "class" => "",
                "heading" => __("Featured", THEMENAME),
                "param_name" => "featured",
                "value" => array (
                    __ ( "Yes, please", THEMENAME ) => true
                ),
                "description" => __("Featured.", THEMENAME)
            ),            
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Price", THEMENAME),
                "param_name" => "price",
                "value" => "",
                "description" => __("Price.", THEMENAME)
            ),
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => __("Content", THEMENAME),
                "param_name" => "content",
                "value" => "",
                "description" => __("Content.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Url", THEMENAME),
                "param_name" => "url",
                "value" => "#",
                "description" => __("Url.", THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Target", THEMENAME),
                "param_name" => "target",
                "value" => array (
                    "_self" => "_self",
                    "_blank" => "_blank",
                ),
                "description" => __("Target.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
