<?php

add_action('init', 'stats_integrateWithVC');

function stats_integrateWithVC() {
    vc_map(array(
        "name" => __("Stats", THEMENAME),
        "base" => "ww-shortcode-stats",
        "class" => "stats",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Timer", THEMENAME),
                "param_name" => "timer",
                "value" => "",
                "description" => __("Timer.", THEMENAME)
            ),         
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Number", THEMENAME),
                "param_name" => "number",
                "value" => "",
                "description" => __("Number.", THEMENAME)
            ),
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => __("Content", THEMENAME),
                "param_name" => "content",
                "value" => "",
                "description" => __("Content.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
