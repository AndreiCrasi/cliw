<?php

add_action('init', 'grid_integrateWithVC');

function grid_integrateWithVC() {
    vc_map(array(
        "name" => __("Grid", THEMENAME),
        "base" => "grid",
        "class" => "grid",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Post Type", THEMENAME),
                "param_name" => "post_type",
                "value" => array(
                    "Post" => "post",
                    "Portfolio" => "portfolio",
                ),
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Filter', THEMENAME),
                "param_name" => "show_filter",
                "value" => array(
                    __("Yes, please", THEMENAME) => 1
                ),
                "description" => __('Show or hide filter on your grid.', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Extra Screen", THEMENAME),
                "param_name" => "col_xs",
                "value" => array(
                    "1 Column" => "col-sx-12",
                    "2 Columns" => "col-sx-6",
                    "3 Columns" => "col-sx-4",
                    "4 Columns" => "col-sx-3",
                    "6 Columns" => "col-sx-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Small Screen", THEMENAME),
                "param_name" => "col_sm",
                "value" => array(
                    "1 Column" => "col-sm-12",
                    "2 Columns" => "col-sm-6",
                    "3 Columns" => "col-sm-4",
                    "4 Columns" => "col-sm-3",
                    "6 Columns" => "col-sm-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Medium Screen", THEMENAME),
                "param_name" => "col_md",
                "value" => array(
                    "1 Column" => "col-md-12",
                    "2 Columns" => "col-md-6",
                    "3 Columns" => "col-md-4",
                    "4 Columns" => "col-md-3",
                    "6 Columns" => "col-md-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns Large Screen", THEMENAME),
                "param_name" => "col_lg",
                "value" => array(
                    "1 Column" => "col-lg-12",
                    "2 Columns" => "col-lg-6",
                    "3 Columns" => "col-lg-4",
                    "4 Columns" => "col-lg-3",
                    "6 Columns" => "col-lg-2",
                ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "style",
                "value" => array(
                    "Style 1" => "style1",
                ),
                "description" => __('Style', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Crop image', THEMENAME),
                "param_name" => "crop_image",
                "value" => array(
                    __("Yes, please", THEMENAME) => true
                ),
                "description" => __('Crop or not crop image on your Post.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Width image', THEMENAME),
                "param_name" => "width_image",
                "description" => __('Enter the width of image. Default: 300.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Height image', THEMENAME),
                "param_name" => "height_image",
                "description" => __('Enter the height of image. Default: 200.', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Title', THEMENAME),
                "param_name" => "show_title",
                "value" => array(
                    __("Yes, please", THEMENAME) => true
                ),
                "description" => __('Show or hide title of post on your grid.', THEMENAME)
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Description', THEMENAME),
                "param_name" => "show_description",
                "value" => array(
                    __("Yes, please", THEMENAME) => true
                ),
                "description" => __('Show or hide description of post on your grid.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt Length', THEMENAME),
                "param_name" => "excerpt_length",
                "value" => '',
                "description" => __('The length of the excerpt, number of words to display.', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "heading" => __('Excerpt More', THEMENAME),
                "param_name" => "excerpt_more",
                "value" => "",
                "description" => __('Excerpt More', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order by', THEMENAME),
                "param_name" => "orderby",
                "value" => array(
                    "None" => "none",
                    "Title" => "title",
                    "Date" => "date",
                    "ID" => "ID"
                ),
                "description" => __('Order by ("none", "title", "date", "ID").', THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "heading" => __('Order', THEMENAME),
                "param_name" => "order",
                "value" => Array(
                    "None" => "none",
                    "ASC" => "ASC",
                    "DESC" => "DESC"
                ),
                "description" => __('Order ("None", "Asc", "Desc").', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
