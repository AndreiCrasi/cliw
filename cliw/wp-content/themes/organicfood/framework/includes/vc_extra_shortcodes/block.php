<?php

add_action('init', 'block_integrateWithVC');

function block_integrateWithVC() {
    vc_map(array(
        "name" => __("Block", THEMENAME),
        "base" => "block",
        "class" => "block",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "style",
                "value" => array(
                    "Style 1" => "style1",
                    "Style 2" => "style2",
                ),
                "description" => __('Style', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title.", THEMENAME)
            ),
            array(
                "type" => "attach_image",
                "class" => "",
                "heading" => __("Image", THEMENAME),
                "param_name" => "image",
                "value" => "",
                "description" => __("Image.", THEMENAME)
            ),            
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => __("Content", THEMENAME),
                "param_name" => "content",
                "value" => "",
                "description" => __("Content.", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
