<?php

add_action('init', 'google_pie_chart_integrateWithVC');

function google_pie_chart_integrateWithVC() {
    vc_map(array(
        "name" => __("Google Chart", THEMENAME),
        "base" => "google-chart",
        "class" => "google-chart",
        "category" => __('Organic Food', THEMENAME),
        "icon" => "of-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Style", THEMENAME),
                "param_name" => "style",
                "value" => array(
                    "Pie Chart" => "pie",
                    "Geo Chart" => "geo"
                ),
                "description" => __('Style', THEMENAME)
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", THEMENAME),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title.", THEMENAME)
            ),           
            array(
                "type" => "textarea",
                "class" => "",
                "heading" => __("Data", THEMENAME),
                "param_name" => "data",
                "value" => "",
                "description" => __("Content.", THEMENAME)
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", THEMENAME),
                "param_name" => "animation",
                "value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
                "description" => __("Animation", THEMENAME)
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", THEMENAME),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", THEMENAME)
            ),
        )
    ));
}
