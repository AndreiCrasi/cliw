<?php
// Adding stripes to rows
vc_add_param ( "vc_row", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Row style", THEMENAME ),
		"admin_label" => true,
		"param_name" => "type",
		"value" => array (
                    "Default" => "ww-default",
                    "Background Video" => "ww-custom-bg-video"
		),
		"description" => __( "", THEMENAME )
) );

vc_add_param ( 'vc_row', array (
		'type' => 'dropdown',
		'heading' => __("Full Width", THEMENAME),
		'param_name' => 'full_width',
		'value' => array (
				"No" => false,
				"Yes" => true
		),
		'description' => __("Row Content Full Width", THEMENAME)
) );

vc_add_param ( "vc_row", array (
		"type" => "colorpicker",
		"class" => "",
		"heading" => __( "Link color", THEMENAME ),
		"param_name" => "row_link_color",
		"value" => "",
		"description" => __( "Select color for link.", THEMENAME )
) );

vc_add_param ( "vc_row", array (
		"type" => "colorpicker",
		"class" => "",
		"heading" => __( "Link color hover", THEMENAME ),
		"param_name" => "row_link_color_hover",
		"value" => "",
		"description" => __( "Select color for link hover.", THEMENAME )
) );

vc_add_param ( "vc_row", array (
		"type" => "checkbox",
		"class" => "",
		"heading" => __( "Same height", THEMENAME ),
		"param_name" => "same_height",
		"value" => array (
				__( "Yes, please", THEMENAME )  => true
		),
		"description" => __( "Set the same hight for all column in this row.", THEMENAME )
) );

vc_add_param ( "vc_row", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Animation", THEMENAME ),
		"admin_label" => true,
		"param_name" => "animation",
		"value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
		"description" => __( "", THEMENAME )
) );

vc_add_param ( "vc_row", array (
		"type" => "checkbox",
		"class" => "",
		"heading" => __( "Enable parallax", THEMENAME ),
		"param_name" => "enable_parallax",
		"value" => array (
				__( "Yes, please", THEMENAME )  => true,
		)
) );

vc_add_param ( "vc_row", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Parallax speed", THEMENAME ),
		"param_name" => "parallax_speed",
		"value" => "0.5"
) );
vc_add_param ( "vc_row", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Video background (mp4)", THEMENAME ),
		"param_name" => "bg_video_src_mp4",
		"value" => "",
		"dependency" => array (
				"element" => "type",
				"value" => array('ww-custom-bg-video')
		)
) );

vc_add_param ( "vc_row", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Video background (ogv)", THEMENAME ),
		"param_name" => "bg_video_src_ogv",
		"value" => "",
		"dependency" => array (
				"element" => "type",
				"value" => array('ww-custom-bg-video')
		)
) );

vc_add_param ( "vc_row", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Video background (webm)", THEMENAME ),
		"param_name" => "bg_video_src_webm",
		"value" => "",
		"dependency" => array (
				"element" => "type",
				"value" => array('ww-custom-bg-video')
		)
) );

vc_add_param ( "vc_column", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Animation", THEMENAME ),
		"admin_label" => true,
		"param_name" => "animation",
		"value" => array(
                    "No" => "",
                    "Top to bottom" => "top-to-bottom",
                    "Bottom to top" => "bottom-to-top",
                    "Left to right" => "left-to-right",
                    "Right to left" => "right-to-left",
                    "Appear from center" => "appear"
                ),
		"description" => __( "", THEMENAME )
) );

vc_add_param ( "vc_column", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Column Align", THEMENAME ),
		"admin_label" => true,
		"param_name" => "column_align",
		"value" => array (
				"None" => "",
				"Left" => "left",
				"Right" => "right",
				"Center" => "center"
		),
		"description" => __( "", THEMENAME )
) );
// Pie chart
vc_add_param ( "vc_pie", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Description", THEMENAME ),
		"admin_label" => true,
		"param_name" => "desc",
		"value" => "",
		"description" => __( "", THEMENAME )
) );
vc_add_param ( "vc_pie", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Style", THEMENAME ),
		"admin_label" => true,
		"param_name" => "style",
		"value" => array (
				"Style 1" => "style1",
				"Style 2" => "style2"
		),
		"description" => __( "Select style for pie.", THEMENAME )
) );
vc_add_param ( "vc_pie", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Icon", THEMENAME ),
		"admin_label" => true,
		"param_name" => "icon",
		"value" => "",
		"description" => __( 'You can find icon class at here: <a target="_blank" href="http://fontawesome.io/icons/">"http://fontawesome.io/icons/</a>. For example, fa fa-heart', THEMENAME )
) );
/*
 * Separator
 */
vc_remove_param('vc_separator','el_class');
vc_add_param ( "vc_separator", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Style Border Width", THEMENAME ),
		"admin_label" => true,
		"param_name" => "border_width",
		"value" => "1",
		"description" => __( "", THEMENAME )
) );
vc_add_param ( "vc_separator", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Show Arrow", THEMENAME ),
		"admin_label" => true,
		"param_name" => "separator_arrow",
		"value" => array (
				"No" => "no",
				"Yes" => "yes"
		),
		"description" => __( "", THEMENAME )
) );
/*
 * Separator with Text
 */
vc_add_param ( "vc_text_separator", array (
		"type" => "textfield",
		"class" => "",
		"heading" => __( "Style Border Width", THEMENAME ),
		"admin_label" => true,
		"param_name" => "border_width",
		"value" => "1",
		"description" => __( "", THEMENAME )
) );
vc_add_param ( "vc_text_separator", array (
		"type" => "textarea_html",
		"class" => "",
		"heading" => __( "Description", THEMENAME ),
		"admin_label" => true,
		"param_name" => "desc",
		"value" => "",
		"description" => __( "", THEMENAME )
) );
/*
 * Progress bar
 */
vc_add_param ( "vc_progress_bar", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Style", THEMENAME ),
		"admin_label" => true,
		"param_name" => "style",
		"value" => array (
				"Default" => "default",
				"Style 1" => "style1"
		),
		"description" => __( "", THEMENAME )
) );
vc_add_param ( "vc_progress_bar", array (
		"type" => "dropdown",
		"class" => "",
		"heading" => __( "Size", THEMENAME ),
		"admin_label" => true,
		"param_name" => "size",
		"value" => array (
				"Small" => "small",
				"Medium" => "medium",
                                "Large" => "large"
		),
                "dependency" => array(
                    "element" => "style",
                    "value" => "style1"
                ),
		"description" => __( "", THEMENAME )
) );
